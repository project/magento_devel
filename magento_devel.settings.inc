<?php
// $Id$

/**
 * @file
 */

function magento_devel_settings() {
  $form = array();

  $form['magento_devel_profilers'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Enable profilers'),
    '#collapsible' => FALSE,
  );

  $form['magento_devel_profilers']['magento_devel_profilers_api'] = array(
    '#type'          => 'checkbox',
    '#title'         => 'Magento API',
    '#description'   => t('Profile API calls'),
    '#default_value' => variable_get('magento_devel_profilers_api', 1)
  );
  $form['magento_devel_profilers']['magento_devel_profilers_api_lowlevel'] = array(
    '#type'          => 'checkbox',
    '#title'         => 'Magento API lowlevel request',
    '#description'   => t('Profile API calls'),
    '#default_value' => variable_get('magento_devel_profilers_api_lowlevel', 0)
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'magento_devel_settings_submit';

  return $form;
}

function magento_devel_settings_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  if ($form_values['magento_devel_profilers_api']) {
    if (!module_exists('devel')) {
      drupal_set_message(t('Strongly recomended to enable <b>developer</b> module'), 'warning');
    }
  }
}
