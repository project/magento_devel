// $Id$

Magento Devel
-------------------------------------------------------------------------------

Provides integration with devel module and adds profiling of Magento API calls.

INSTALLATION
-------------------------------------------------------------------------------

Install as usual, see http://drupal.org/node/70151 for further information.
Go to admin/settings/magento/devel to configure the settings.

Maintainers
-------------------------------------------------------------------------------
Module is fully developpped, sponsored and maintained by Adyax.

Originally developed by:
Andrei Kovalevski - akovalevski@adyax.com
