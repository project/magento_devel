<?php
// $Id$

/**
 * @file
 * Magento API calls profiler
 */

define('MAGENTO_DEVEL_PROFILER_SESSION_NAME', 'magento_devel_session');

function magento_devel_profiler_add($name = NULL, $point = NULL, $args = array(), $clean = FALSE) {
  static $profiler_tree;
  static $branches_stack;
  static $plain;
  static $counter;
  if (!$name) {
    $result = isset($profiler_tree) ? $profiler_tree['child'] : array();
  }

  if (!isset($profiler_tree) || $clean) {
    $profiler_tree = array('child' => array());
    $branches_stack= array(& $profiler_tree);
  }

  if (!$name) {
    return $result;
  }
  $branch = & $branches_stack[0];
  if ($plain) {
    $next = (count($branch["child"]) + 1) . '. ' . $name;
    $profiler_tree['child'][$next] = array(
      'name' => $name,
      'time' => date('Y-m-d H:n:s'),
      'duration' => 'disabled',
      'args' => $args
    );

    if ($point == 'after') {
      $profiler_tree[count($profiler_tree) - 1]['child']['result'] = $args[0];
    }
    else {
      $profiler_tree[count($profiler_tree) - 1]['child']['args'] = $args;
    }

    return;
  }

  if ($point) {
    if ($point == 'before') {
      $next = (count($branch["child"]) + 1) . '. ' . $name;
      $counter_name = 'magento_devel_profiler-' . (++$counter);
      $branch["child"][$next] = array(
        'name' => $name,
        'time' => date('Y-m-d H:n:s'),
        'duration' => $counter_name,
        'args' => $args,
      );
      array_unshift($branches_stack, $branch['child'][$next]);
      timer_start($counter_name);
    }
    elseif ($point == 'after') {
      //tree is broken
      if ($branch['name'] != $name) {
        $plain= TRUE;
        magento_devel_profiler_add($name, $point, $args);
      }
      else {
        $timer_result = timer_stop($branch['duration']);
        $branch['duration'] = $timer_result['time'] / 1000;
        $branch['result'] = $args[0];
        array_shift($branches_stack);
      }
    }
  }
  else {
    $branch['child'][] = array(
      'name' => $name,
      'args' => $args,
    );
  }
}

/**
 * Save current profiler info in session
 */
function magento_devel_profiler_session_save() {
  if (!isset($_SESSION[MAGENTO_DEVEL_PROFILER_SESSION_NAME])) {
    $_SESSION[MAGENTO_DEVEL_PROFILER_SESSION_NAME] = array();
  }
  $tree = magento_devel_profiler_add();
  if ($tree) {
    $next = (count($_SESSION[MAGENTO_DEVEL_PROFILER_SESSION_NAME]) + 1) . '. ' . $_GET['q'];
    $_SESSION[MAGENTO_DEVEL_PROFILER_SESSION_NAME][$next] = $tree;
  }
}

/**
 * Load current profiler info from session
 *
 * @param bool $clear
 *   Clear session after fetching result
 */

function magento_devel_profiler_session_load($clear = FALSE) {
  if (!isset($_SESSION[MAGENTO_DEVEL_PROFILER_SESSION_NAME])) {
    $result = NULL;
  }
  else {
    $result = isset($_SESSION[MAGENTO_DEVEL_PROFILER_SESSION_NAME]) ? $_SESSION[MAGENTO_DEVEL_PROFILER_SESSION_NAME] : NULL;
    if ($clear) {
      unset($_SESSION[MAGENTO_DEVEL_PROFILER_SESSION_NAME]);
    }
  }

  return $result;
}
